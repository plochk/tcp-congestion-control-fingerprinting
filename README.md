# TCP Congestion Control Fingerprinting
Paper written in the seminar "Innovative Internet Technologies and Mobile Communications" 2021 at Technische Universität München Chair of Network Architectures and Services.
## Abstract
In order to make judgements on the spread of
certain congestion control algorithms a way to fingerprint
the algorithm of a host is needed. This can be done with
**congestion control identification (CCI)** algorithms. This work
presents the general approach of such an algorithm and
summarizes possible categorizations for CCI. It presents
Congestion Avoidance Algorithm Identification (CAAI) as an
active and DeePCCI as a passive example. In an accuracy
evaluation over a WAN connection these algorithms are
compared to each other and to a more recent approach
from 2020, Inspector Gadget (IG), which includes further
optimizations. IG shows near perfect accuracy, DeePCCIs
and CAAIs accuracies are rather humble, former with 90-
92% and latter with 41-94%, and to conclude we explore
how these results come to be.
